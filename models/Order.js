const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId required"]
	},
	products: [
		{
			productName: {
				type: String,
				required: [true, "productId required"]
			},
			price: {
				type: Number
			},
			quantity: {
				type: Number,
				required: [true, "Quantity required"]
			}
		}
	],
	totalAmount: {
		type: Number
	},
	puchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);