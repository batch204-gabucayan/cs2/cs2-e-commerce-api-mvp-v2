const Product = require("../models/Product");
const bcrypt = require("bcrypt");
// const auth = require("../auth");

// Create a new Product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// Retrieving all Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving all products
module.exports.getAll = () => {
	return Product.find().then(result => {
		// console.log(result)
		return result;
	})
}

// Retrieving single/specific Product
module.exports.getProduct = (reqParams) => {
	// console.log(reqParams);
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

// Updating Product information *ADMIN only
module.exports.updateProduct = (reqParams, reqBody, data) => {
		let updatedProduct = {
			productName: reqBody.productName,
			description: reqBody.description,
			price: reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})	
}

// Archiving Product
module.exports.archiveProduct = (reqParams, reqBody) => {
	// console.log(reqParams);
	let update = {
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, update).then((product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

// Activate Product
module.exports.activateProduct = (reqParams) => {
	// console.log(reqParams);
	let update = {
		isActive: true
	}
	return Product.findByIdAndUpdate(reqParams.productId, update).then((product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}