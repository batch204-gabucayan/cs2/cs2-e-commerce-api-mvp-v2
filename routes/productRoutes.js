const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');
// const Product = require("../models/Product");

// Route for creating/add a Product
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// console.log(userData.isAdmin);

	if(userData.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});

// Route for Retrieving all Active Products
router.get("/allActive", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retieving all products
router.get("/all", (req, res) => {
	productController.getAll().then(resultFromController => res.send(resultFromController))
});

// Route for Retrieving single/specific product
router.get("/:productId", (req, res) => {
	// console.log(req.params);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update Product Information *ADMIN only
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});

// Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});

// Activate Product
router.put("/:productId/activate", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});
module.exports = router;